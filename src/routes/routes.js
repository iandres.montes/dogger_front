import { lazy } from 'react';

const routes = [
  {
    path: '',
    component: lazy(() => import('features/Dashboard')),
    exact: true,
  },
  {
    path: 'add-dogs',
    component: lazy(() => import('features/AddDogs')),
    exact: true,
  },
  {
    path: 'assign-walker/:dog_id',
    component: lazy(() => import('features/AssignWalker')),
    exact: true,
  },
];

export default routes;
