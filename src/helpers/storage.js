
// Guardar datos en general, en especial del login para dogger
export let save = (key, value) => localStorage.setItem(key, value)

// Retornar informacion guardada en localstorage para la persistencia de datos en la app dogger
export let get = (key) => localStorage.getItem(key)