import { useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Form, Button, Alert, Input } from 'antd';
import {postRequest} from 'app/axiosClient' 
import Select from 'react-select'

function AddDogs() {
  let history = useHistory();
  const {idUser} = useSelector(state => state.authentication);
  const [error, setError] = useState({state:false, log:''});
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState('');
  const [selected, setSelected] = useState([{ value: 'guest', label: 'Usuario' }]);

  const options = [
    { value: 'P', label: 'Pequeño' },
    { value: 'M', label: 'Mediano' },
    { value: 'G', label: 'Grande' },
  ]

  const save_dog = () =>{
    setIsLoading(true)
    try{
      postRequest('dogs/api/create', {name, size:selected.value, guest_id:idUser }).then(res =>{
        if(res.data){
          return history.push('/')
        }else{
          
        }
      })
    }catch{
      setError({state:true, log:'Algo salio mal :c'})
    }
  }

  return (
    <div className="App" style={{padding:30}}>
      <Form
        name="normal_add_dog"
        className="form"
        initialValues={{
          remember: true,
        }}
        onFinish={save_dog}
      >
        {error.state ? 
          <Alert
            style={{marginBottom:15}}
            message="Error en el registro"
            description={error.log}
            type="error"
          />
        :
          null
        }

        <h1>Agregar perrito</h1>

        <Form.Item
          name="Email"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa el nombre del perrito!',
            },
          ]}
        >
          <Input size="large"
            placeholder="Nombre del perrito"
            type='text'
            value={name}
            onChange={(e)=>setName(e.target.value)}
          />
        </Form.Item>

        <Form.Item
          name="selectType"
          rules={[
            {
              required: true,
              message: 'Por favor, selecciona el tamaño de tu perrito',
            },
          ]}
        >
          <Select options={options} onChange={setSelected} />
        </Form.Item>

        <Form.Item>
          <Button loading={isLoading} type="primary" htmlType="submit" className="login-form-button"
            size="large">Guardar Perrito
          </Button>
        </Form.Item>

      </Form>
    </div>
  );
}

export default AddDogs;
