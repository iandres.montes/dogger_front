import { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {Link} from 'react-router-dom';
import { Card, Skeleton, Button, Divider, Descriptions } from 'antd';
import {getRequest, deleteRequest} from 'app/axiosClient' 

let Dashboard = () => {
  let history = useHistory();
  const {idUser} = useSelector(state => state.authentication);
  const [dogs, setDogs] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  let _loadDogs = () =>{
    try {
      getRequest(`dogs/api/byGuest/${idUser}`).then(res => {
        let data = res.data;
        if (!data.error){
          if(data.response.length > 0){
            setDogs(data.response)
            setIsLoading(false)
          }else{
            // setIsLoading(false)
          }
        }
      });
      // yield put(loginSuccess(response.data));
    } catch (e) {
      console.log(e);
      // yield put(loginFailure());
    }
  }

  const delete_dog = (id) =>{
    setIsLoading(true)
    try{
      deleteRequest(`dogs/api/${id}/delete`).then(res =>setDogs([]))
    }catch(e){
      console.log(e);
    }
  }

  let _getWalker = (walker) =>{
    if(walker){
      return `${walker.name} ${walker.lastname} - ${walker.mobile}`
    }else{
      return 'Sin paseador asingado'
    }
  }

  useEffect(()=>{if(dogs.length === 0)_loadDogs()})

  return (
    <div className="App" style={{padding:30}}>
      <Card key="card_gods" title="Perritos guardados" extra={<Link to="/add-dogs">Agregar Perro</Link>}>
        <Skeleton loading={isLoading} active>
          {dogs.length > 0 ?
            dogs.map(({id,name, size, walker}) =>{
              let getWalker = _getWalker(walker);
              let getSize = {
                P:'Pequeño',
                M:'Mediano',
                G:'Grande',
              }
              return (
                <>
                  <Descriptions title={`Perfil de ${name}`}>
                    <Descriptions.Item label="id">{id}</Descriptions.Item>
                    <Descriptions.Item label="size">{getSize[size]}</Descriptions.Item>
                    <Descriptions.Item label="walker">{getWalker}</Descriptions.Item>
                  </Descriptions>,
                  <div key={`div_${id}`} style={{marginTop:15}}>
                    <Button key={`del_${id}`} style={{marginRight:10}} type="danger" onClick={()=>delete_dog(id)}>
                      Eliminar Perrito
                    </Button>
                    <Button key={`assign_${id}`} type="primary" onClick={()=>history.push(`/assign-walker/${id}`)}>Asingar Paseador</Button>
                  </div>
                  <Divider key={`divider_${id}`} />
                </>
              )
            })
          :
            null
          }
        </Skeleton>
      </Card>
      
    </div>
  );
}

export default Dashboard;
