import { useState, useEffect } from 'react';
// import { useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { Form, Button, Alert, Input, Skeleton } from 'antd';
import {getRequest, putRequest} from 'app/axiosClient' 
import Select from 'react-select'

function AssignWalker() {
  let history = useHistory();
  let { dog_id } = useParams();
  const [error, setError] = useState({state:false, log:''});
  const [isLoading, setIsLoading] = useState(false);
  const [profile, setProfile] = useState({name:''});
  const [selected, setSelected] = useState([{ value: 'guest', label: 'Usuario' }]);
  const [options, setOptions] = useState([]);

  const save_dog = () =>{
    setIsLoading(true)
    try{
      let dog = profile;
      dog.walker = selected.value.id;
      putRequest(`dogs/api/byID/${dog_id}`, dog).then(res =>{
        let data = res.data;
        if(!data.error){
          console.log(res.data);
          return history.push('/')
        }else{
          setError({state:true, log:data.response.message})
          setIsLoading(false)
        }
      })
    }catch{
      setError({state:true, log:'Algo salio mal :c'})
    }
  }

  let getDogProfile = () =>{
    setIsLoading(true)
    try{
      getRequest(`dogs/api/byID/${dog_id}`).then(res =>{
        let data = res.data;
        if(!data.error){
          setProfile(data.response)
          setIsLoading(false)
          // return history.push('/')
        }else{
          
        }
      })
    }catch{
      setError({state:true, log:'Algo salio mal :c'})
    }
  }

  let getWalkers = () =>{
    setIsLoading(true)
    try{
      getRequest('walker/api').then(res =>{
        let data = res.data;
        if(data){
          let setList = []
          data.forEach(item => {
            setList.push({
              value: item, 
              label: ` ${item.name} ${item.lastname}`,
            })
          });
          setOptions(setList)
          setIsLoading(false)
          // return history.push('/')
        }else{
          
        }
      })
    }catch{
      setError({state:true, log:'Algo salio mal :c'})
    }
  }

  useEffect(()=>{if(profile.name.length <= 0){getDogProfile();getWalkers()}})

  return (
    <div className="App" style={{padding:30}}>
      <Skeleton loading={isLoading} active>
        <Form
          name="normal_add_dog"
          className="form"
          initialValues={{
            remember: true,
          }}
          onFinish={save_dog}
        >
          {error.state ? 
            <Alert
              style={{marginBottom:15}}
              message="Error en el registro"
              description={error.log}
              type="error"
            />
          :
            null
          }

          <h1>Asifgnar paseador para el perrito</h1>

          <Form.Item
            name="Email"
          >
            <Input size="large"
              placeholder={profile.name}
              type='text'
              disabled
              value={profile.name}
              // onChange={(e)=>setName(e.target.value)}
            />
          </Form.Item>

          <Form.Item
            name="selectType"
            rules={[
              {
                required: true,
                message: 'Por favor, selecciona el tamaño de tu perrito',
              },
            ]}
          >
            <Select options={options} onChange={(item)=>{setSelected(item)}} />
          </Form.Item>

          <Form.Item>
            <Button loading={isLoading} type="primary" htmlType="submit" className="login-form-button"
              size="large">Asignar Paseador
            </Button>
          </Form.Item>

        </Form>
      </Skeleton>
    </div>
  );
}

export default AssignWalker;
