import { useState } from 'react';
import {useDispatch} from 'react-redux'
import {useSelector} from 'react-redux'
import {Form, Input, Button, Alert} from 'antd';
import {UserOutlined, LockOutlined, MailOutlined, PhoneOutlined} from '@ant-design/icons';
import { register } from './authenticationSlice'
import Select from 'react-select'


let RegisterPage = () => {
  const loader = useSelector(state => state.authentication.loader);
  const error = useSelector(state => state.authentication.error);
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [mobile, setMobile] = useState('');
  const [selected, setSelected] = useState([{ value: 'guest', label: 'Usuario' }]);
  
  const options = [
    { value: 'guest', label: 'Usuario' },
    { value: 'walker', label: 'Paseador' },
  ]

  let _register = () =>{
    dispatch(register(
      {name, lastname, email, password, mobile, selected},
    ))

  }

  return (
    <div className="container">
      <Form
        name="normal_login"
        className="form"
        initialValues={{
          remember: true,
        }}
        onFinish={_register}
      >
        {error.state ? 
          <Alert
            style={{marginBottom:15}}
            message="Error en el registro"
            description={error.log}
            type="error"
          />
        :
          null
        }
        <Form.Item
          name="selectType"
          rules={[
            {
              required: true,
              message: 'Por favor, selecciona tu perfil!',
            },
          ]}
        >
          <Select options={options} onChange={setSelected} />
        </Form.Item>

        <Form.Item
          name="Nombre"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa tu nombre!',
            },
          ]}
        >
          <Input size="large"
            prefix={<UserOutlined className="site-form-item-icon"/>}
            placeholder="Nombre"
            autoComplete="username"
            value={name}
            onChange={(e)=>setName(e.target.value)}
          />
        </Form.Item>

        <Form.Item
          name="Apellido"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa tu apellido!',
            },
          ]}
        >
          <Input size="large"
            prefix={<UserOutlined className="site-form-item-icon"/>}
            placeholder="Apellidos"
            autoComplete="username"
            value={lastname}
            onChange={(e)=>setLastname(e.target.value)}
          />
        </Form.Item>
        
        <Form.Item
          name="Email"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa tu email!',
            },
          ]}
        >
          <Input size="large"
            prefix={<MailOutlined className="site-form-item-icon"/>}
            placeholder="Email"
            type='email'
            autoComplete="email"
            value={email}
            onChange={(e)=>setEmail(e.target.value)}
          />
        </Form.Item>

        <Form.Item
          name="Contraseña"
          rules={[
            {
              required: true,
              message: 'Por favor, escribe tu contraseña!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon"/>}
            type="password"
            placeholder="Contraseña"
            size="large"
            autoComplete="current-password"
            value={password}
            onChange={(e)=>setPassword(e.target.value)}
          />
        </Form.Item>

        <Form.Item
          name="Telefono"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa tu Telefono!',
            },
          ]}
        >
          <Input size="large"
            prefix={<PhoneOutlined className="site-form-item-icon"/>}
            placeholder="3005124578"
            type='phone'
            autoComplete="phone"
            value={mobile}
            onChange={(e)=>setMobile(e.target.value)}
          />
        </Form.Item>

        <Form.Item>
          <Button loading={loader} type="primary" htmlType="submit" className="login-form-button"
            size="large">Registrar
          </Button>
        </Form.Item>

        <Form.Item>
          <Button loading={loader} type="second" htmlType="button" onClick={event =>  window.location.href='/login'} className="login-form-button"
            size="large">Login
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default RegisterPage;
