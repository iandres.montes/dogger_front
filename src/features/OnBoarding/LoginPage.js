import { useState } from 'react';
import {useDispatch, useSelector} from 'react-redux'
import {Form, Input, Button, Checkbox, Alert} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import { login } from './authenticationSlice'
import Select from 'react-select'

let LoginPage = () => {
  const loader = useSelector(state => state.authentication.loader);
  const error = useSelector(state => state.authentication.error);
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [selected, setSelected] = useState([{ value: 'guest', label: 'Usuario' }]);
  
  const options = [
    { value: 'guest', label: 'Usuario' },
    // { value: 'walker', label: 'Paseador' },
  ]

  let _login = () =>{
    dispatch(login(
      {email, password, selected},
    ))
  }

  return (
    <div className="container">
      <Form
        name="normal_login"
        className="form"
        initialValues={{
          remember: true,
        }}
        onFinish={_login}
      >
        {error.state ? 
          <Alert
            style={{marginBottom:15}}
            message="Error en el registro"
            description={error.log}
            type="error"
          />
        :
          null
        }

        <Form.Item
          name="selectType"
          rules={[
            {
              required: true,
              message: 'Por favor, selecciona tu perfil!',
            },
          ]}
        >
          <Select options={options} onChange={setSelected} />
        </Form.Item>
        
        <Form.Item
          name="Email"
          rules={[
            {
              required: true,
              message: 'Por favor, ingresa tu email!',
            },
          ]}
        >
          <Input size="large"
            prefix={<UserOutlined className="site-form-item-icon"/>}
            placeholder="Email"
            type='email'
            autoComplete="email"
            value={email}
            onChange={(e)=>setEmail(e.target.value)}
          />
        </Form.Item>
        <Form.Item
          name="Contraseña"
          rules={[
            {
              required: true,
              message: 'Por favor, escribe tu contraseña!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon"/>}
            type="password"
            placeholder="Contraseña"
            size="large"
            autoComplete="current-password"
            value={password}
            onChange={(e)=>setPassword(e.target.value)}
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
        </Form.Item>

        <Form.Item>
          <Button loading={loader} type="primary" htmlType="submit" className="login-form-button"
            size="large">Log in
          </Button>
        </Form.Item>

        <Form.Item>
          <Button loading={loader} type="second" htmlType="button" onClick={event =>  window.location.href='/register'} className="login-form-button"
            size="large">Registrate
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default LoginPage;
