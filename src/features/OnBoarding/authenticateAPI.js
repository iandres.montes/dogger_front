import {all, call, put, takeLatest} from 'redux-saga/effects'
import { login, register, loginSuccess, registerSuccess, loginFailure } from './authenticationSlice';
import { postRequest } from 'app/axiosClient'

function* loginAPI(action) {
  
  try {
    let data = action.payload
    const response = yield call(() => postRequest(`${data.selected.value}/api/login`, data));
    yield put(loginSuccess(response.data));
  } catch (e) {
    yield put(loginFailure());
  }
}

function* RegisterAPI(action) {
  try {
    let data = action.payload
    const response = yield call(() => postRequest(`${data.selected.value}/api/register`, data));
    if(data.selected.value === 'guest'){
      yield put(registerSuccess(response.data));
    }else{
      // return (<Redirect to='/login' />)
      window.location.href = '/login'
    }
  } catch (e) {
    yield put(loginFailure());
  }
}


export default function* rootSaga() {
  yield all([takeLatest(login, loginAPI), takeLatest(register, RegisterAPI)]);
}
