import { createSlice } from '@reduxjs/toolkit'
import {get, save} from 'helpers/storage'


let loadingInitialState = () =>{
  let isAuthenticated = (get('isAuthenticated') === 'true')
  let idUser = get('idUser') ? parseInt(get('idUser')) : null

  return {
    isAuthenticated,
    idUser,
    loader: false,
    error:{state:false, log:''},
  }
}

const initialState = loadingInitialState()

const authenticationSlice = createSlice({
  name: 'authentication',
  initialState,
  reducers: {
    login(state) {
      state.loader = true;
    },
    loginSuccess(state, action) {
      let data = action.payload
      if (data.error){
        state.loader = false;
        state.isAuthenticated = false;
        state.error.state = true;
        state.error.log = data.response.message;
      }else{
        state.loader = false;
        state.isAuthenticated = true
        state.idUser = data.response.id
        save('isAuthenticated', true)
        save('idUser', data.response.id)
      }
    },
    register(state) {
      state.loader = true;
    },
    registerSuccess(state, action) {
      let data = action.payload
      if (data.error){
        state.loader = false;
        state.isAuthenticated = false;
        state.error.state = true;
        state.error.log = data.response.message;
      }else{
        state.loader = false;
        state.isAuthenticated = true
        state.idUser = data.response.id
        save('isAuthenticated', true)
        save('idUser', data.response.id)
      }
    },
    loginFailure(state) {
      state.loader = false;
      state.isAuthenticated = false;
    },
  },
});

export const { login, register, loginSuccess, registerSuccess, loginFailure } = authenticationSlice.actions;
export default authenticationSlice.reducer;
